<!DOCTYPE HTML>
<title>Edit Books</title>
<link href="navBar.css" rel="stylesheet">
<style>
   

body {
    margin:0;
    padding:0;
    font-family: sans-serif;
    color: white;
    text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;
    background-image: url('library.jpg');
    background-size: cover;
}

table {
  
  position: absolute;
  top: 50%;
  left: 50%;
  width: 600px;
  padding: 30px;
  transform: translate(-50%, -50%);
  background: rgba(0,0,0,.5);
  box-sizing: border-box;
  box-shadow: 0 15px 25px rgba(0,0,0,.6);
  border-radius: 10px;

}

</style>

<html>
<head>
<ul>
  <li><a class="active" href="SearchBooks.php">Home</a></li>
  <li><a href="EnterBooks.php">Enter Books</a></li>
  <li><a href="DisplayBooks.php">Library</a></li>
</ul>
</head>
<body>
    <?php
    require ("DBConnection.php");
    $isbn=$_GET["isbn"];
    $query = "select * from books where isbn = '$isbn'";
    $result = mysqli_query($db,$query);
    //var_dump($isbn,$title,$author,$edition,$publication);

    if($row = mysqli_fetch_assoc($result)){
        $isbn=$row["isbn"];
        $title=$row["title"];
        $author=$row["author"];
        $edition=$row["edition"];
        $publication=$row["publication"];
    
    ?>
    <center><h2>Book Search System</h2></center>
    <!--Once the form is submitted, all the form data is forwarded to InsertBooks.php -->
    <form action="update.php" method="post">
 
        <table border="2" align="center" cellpadding="5" cellspacing="5">
            <tr>
            <td> Enter ISBN :</td>
            <td> <input type="text" name="isbn" size="48" value="<?php echo $isbn ?>"> </td>
            </tr>
            <tr>
            <td> Enter Title :</td>
            <td> <input type="text" name="title" size="48" value="<?php echo $title ?>" > </td>
            </tr>
            <tr>
            <td> Enter Author :</td>
            <td> <input type="text" name="author" size="48" value="<?php echo $author ?>"> </td>
            </tr>
            <tr>
            <td> Enter Edition :</td>
            <td> <input type="text" name="edition" size="48" value="<?php echo $edition ?>"> </td>
            </tr>
            <tr>
            <td> Enter Publication: </td>
            <td> <input type="text" name="publication" size="48" value="<?php echo $publication ?>"> </td>
            </tr>
            <tr>
            <td></td>
            <td>
            <input type="submit" value="Submit">
            <input type="reset" value="Reset">
            </td>
            </tr>
        </table>
    </form>
    <?php
    }
    else{
        echo "<script>document.location.href='DisplayBooks.php';</script>";
    }
    ?>
</body>
</html>