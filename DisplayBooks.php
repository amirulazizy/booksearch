<!DOCTYPE HTML>
<title>Library</title>
<link href="navBar.css" rel="stylesheet">
<html>
<style>
body {
       
       margin:0;
       padding:0;
       font-family: sans-serif;
       color: white;
       text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;
       background-image: url('library.jpg');
       background-size: cover;
   }
   
   table {
     /* display: flex;*/
     /* top: 70%;
     left: 50%; */ 
     width: 1150px;
     padding: 30px;
     /* transform: translate(-50%, -50%); */
     background: rgba(0,0,0,.5);
     box-sizing: border-box;
     box-shadow: 0 15px 25px rgba(0,0,0,.6);
     border-radius: 10px;
   
   }

   </style>
   <head>
        <ul>
            <li><a class="active" href="SearchBooks.php">Home</a></li>
            <li><a href="EnterBooks.php">Enter Books</a></li>
            <li><a href="DisplayBooks.php">Library</a></li>
        </ul>
    </head>
    <body>
    <center><h2>Book Search System</h2></center>
    
 
    <?php
    include("DBConnection.php");
 
    $search = $_REQUEST["search"] ?? null ;
 
    $query = "select * from books where title like '%$search%'" ;
    
    // $query = "select * from books"; //search with a book name in the table book_info
    $result = mysqli_query($db,$query);
    
    //try
    //$id = $_GET['delete'];
    //mysqli_query($search,"DELETE FROM books WHERE id='".$id."'");

    if($result){
        if(mysqli_num_rows($result) > 0 )
        {
        ?>
        <table border="2" align="center" cellpadding="5" cellspacing="5">
            <tr>
                <th> ISBN </th>
                <th> Title </th>
                <th> Author </th>
                <th> Edition </th>
                <th> Publication </th>
                <th> Action </th>
            </tr>
 
            <?php while($row = mysqli_fetch_assoc($result))
            {
                $isbn = $row["isbn"];
                $title = $row["title"];
                $author = $row["author"];
                $edition = $row["edition"];
                $publication = $row["publication"];
            ?>
            <tr>
                <td><?php echo $isbn?> </td>
                <td><?php echo $title?> </td>
                <td><?php echo $author?> </td>
                <td><?php echo $edition?> </td>
                <td><?php echo $publication?> </td>
                <td>
                <?php 
                    // echo "<a href='EditBooks.php'>update     </a>";
                    // echo "<a href='delete.php?isbn=".$isbn."'>delete</a>";
                    echo "<button class=\"btn\" type=\"button\" name=\"update\" style=\"background: #FFFFFF; margin:5px;\" onclick=\"document.location='EditBooks.php?isbn=$isbn'\">update</button>";
                    echo "<button class=\"btn\" type=\"button\" name=\"delete\" style=\"background: #FF0000; margin:5px;\" onclick=\"document.location='delete.php?isbn=$isbn'\">delete</button>";
                ?>    
                </td><!-- <button class="btn" type="button" name="delete" style="background: #FF0000;" onclick="document.location='delete.php?isbn=".$isbn.">delete</button></td> -->
            </tr>
        <?php
            }
        }
    }
 
    
    else
        echo "<center>No books found in the library by the name $search </center>" ;
    ?>   
        </table>
    </body>
</html>